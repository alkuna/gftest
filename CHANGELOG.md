# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
 - Function 4.

## [0.2.0] - 2017-02-27

## [0.1.2] - 2017-02-24
### Added
 - Hotfix 2

## [0.1.1] - 2017-02-09
### Fixed
 - something ...

## [0.1.0] - 2017-02-09
### Added
 - myfeature ...

[Unreleased]: master..dev
[0.2.0]: v0.1.2..v0.2.0
[0.1.2]: v0.1.1..v0.1.2
[0.1.1]: v0.1.0..v0.1.1
[0.1.0]: v0.0.0..v0.1.0
